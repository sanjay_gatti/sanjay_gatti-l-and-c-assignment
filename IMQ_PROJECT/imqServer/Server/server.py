import sys
sys.path.append('../')
import socket
import _thread
from Sockets.sockethandler import Socket_Handler
from Sockets.crypter import Crypter
from Protocols.protocalparser import Parser
from Protocols.request import Request
from Protocols.response import Response
from Database.database import Database
from Handlers.files import File
from Log.log import logger
from Server.server_services import Server_Services
from Exception_Handler.exception_handler import Exception_Handling
exception_handling=Exception_Handling()
db=Database()
write_file=File()
service=Server_Services()

class server_socket(Socket_Handler):
    def __init__(self):
        super().__init__()
    
    def server_accept(self):
        while True:
            global client_address
            self.client, self.address=self.socket_accept()
            client_address = self.address[0]+'_'+str(self.address[1])
            print("connected to : "+self.address[0]+str(self.address[1]))
            _thread.start_new_thread(self.client_thread_communicate,(self.client,self.address,client_address))
        server.socket_close()
    
    def client_thread_communicate(self,connection,address,client_address):
        response_to_send="Connection made with "+str(client_address)
        parsed_header= self.data_encode(response_to_send)
        connection.send(str.encode(parsed_header))
        user_name=connection.recv(2048)
        user_name=self.data_decode(user_name)
        db.insert_into_reference_table(client_address,user_name)
        try:
            while True:
                data=connection.recv(2048)
                if not data:
                    break
                else:
                    self.data_processing(data,connection,user_name)
            connection.close()
        except socket.error as e:
            exception_handling.connection_exception(str(e))

    def data_processing(self,data,connection,user_name):
        response=self.data_decode(data)
        result=service.check_operation(user_name,response)
        parsed_header= self.data_encode(result)
        connection.send(str.encode(parsed_header))
    
    def data_encode(self,original_data):
        request_header = Response(original_data)
        encoded_data = Parser.get_json_encoded(self,request_header)
        return encoded_data
    
    def data_decode(self,encoded_data):
        parsed_response = Parser.get_json_decoded(self,encoded_data)
        response = parsed_response['data']
        return response

if __name__ == '__main__':
    try:
        db.create_clients_reference_table()
        server=server_socket()
        server.create_socket()
        server.socket_bind()
        server.socket_listen()
        server.server_accept()
    except socket.error as e:
        print(str(e))
        logger.exception(str(e))

    


    






