import sys
import numpy as np
sys.path.append('../')
from datetime import datetime
from Database.database import Database
from Sockets.sockethandler import Socket_Handler
from Constants.constants import message_status,divider
db=Database()
message_queue={}
class Server_Services:
    def check_operation(self,user_name,response):
        response=response.split(":")
        if(response[0]==str(1)):
            result=db.creating_channel(str(response[1]),user_name)
        elif(response[0]==str(2)):
            result=db.add_subscriber(user_name,response[1])
        elif(response[0]==str(3)):
            result=db.update_message_table(response[1],response[2])
            self.push(str(response[1]),str(response[2]),user_name)
        elif(response[0]==str(4)):
            result=db.show_list_of_channel()
        elif(response[0]==str(5)):
            result=self.pull(str(user_name),str(response[2]))
            db.update_last_seen(str(user_name),str(response[2]))
        elif(response[0]==str(6)):
            result=db.return_subscribed_channels(str(user_name))
        return result

    def push(self,channel_ID,message,user_name):
        now = datetime.now()
        push_time = now.strftime("%Y-%m-%d %H:%M:%S")
        data=message+"="+str(push_time)
        if channel_ID in message_queue.keys():
            old_data=message_queue.get(channel_ID)
            old_data=old_data+divider+data
            message_queue[channel_ID]=old_data
        else:
            message_queue[channel_ID]=data
        print(message_queue)

    def pull(self,user_name,channel_ID):
        id=db.return_client_id(user_name)
        time =db.return_last_seen(channel_ID,id)
        time=str(time[0]).replace('T',' ')
        time=time[:-10]   
        last_seen=datetime.strptime(str(time), '%Y-%m-%d %H:%M:%S')
        data=message_queue.get(channel_ID)
        if channel_ID not in message_queue.keys():
            return "CHANNEL IS EMPTY\n"
        else:
            data=data.split(divider)
            result=''
            for i in data:
                i=i.split("=")
                message_Time=datetime.strptime(str(i[1]), '%Y-%m-%d %H:%M:%S')
                print("Message_Time: "+str(message_Time))
                print("last_seen: "+str(last_seen))
                if(message_Time>=last_seen):
                    result=result+"Message '"+str(i[0])+"' received at : "+str(message_Time)+".\n"
               
            if result == '':
                return "NO MESSAGES FOR YOU\n"
            else:
                return result




         



