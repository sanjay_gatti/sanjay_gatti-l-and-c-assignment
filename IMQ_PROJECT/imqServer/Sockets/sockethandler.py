import sys
sys.path.append('../')
import socket
import _thread
from Sockets.crypter import Crypter
from Constants.constants import port_number,limit
from Database.database import Database
from Handlers.files import File
from Log.log import logger
from Protocols.protocalparser import Parser
from Protocols.request import Request
from Exception_Handler.exception_handler import Exception_Handling
exception_handling=Exception_Handling()
serversocket = socket.socket()
global threadCount
db=Database()
write_file=File()

class Socket_Handler():

    def __init__(self):
        self.host= socket.gethostname()
        self.port=port_number

    def create_socket(self):
        try:
            self.socket_object = socket.socket()
            return True
        except socket.error as e:
            exception_handling.create_socket_exception(str(e))
            return False
    
    def socket_bind(self):
        try:
            self.socket_object.bind((self.host,self.port))
            return True
        except socket.error as e:
            exception_handling.socket_bind_exception(str(e))
            self.socket_object.close()
            return False

    def socket_listen(self):
        try:
            print("waiting for connection!!")
            self.socket_object.listen(limit)
            return True
        except socket.error as e:
            exception_handling.socket_listen_exception(str(e))
            self.socket_object.close()
            return False

    def connect_socket(self):
        print("waiting!!")
        try:
            self.socket_object.connect((self.host,self.port))
            return True
        except socket.error as e:
            exception_handling.connect_socket_exception(str(e))
            return False

    def socket_accept(self):
        try:
            client, address= self.socket_object.accept()
            return client,address
            return True
        except socket.error as e:
            exception_handling.socket_accept_exception(str(e))
            return False

    def socket_close(self):
        try:
             self.socket_object.close()
             print('Connection closed!!')
             return True
        except socket.error as e:
            exception_handling.socket_close_exception (str(e))

    def recv_response(self):
        try:
             response = self.socket_object.recv(1024)
             parsed_response = Parser.get_json_decoded(self,response)
             response_message=parsed_response["data"]
             return response_message
        except socket.error as e:
            exception_handling.recv_response_exception(str(e))
            self.socket_object.close()

    def send_response(self,request):
        try:
             request_header = Request(request)
             parsed_header = Parser.get_json_encoded(self,request_header)
             self.socket_object.send(str.encode(parsed_header))
        except socket.error as e:
            exception_handling.se(str(e))
            self.socket_object.close()

       


