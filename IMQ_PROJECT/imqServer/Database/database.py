import mysql.connector
import sys
sys.path.append('../')
from Log.log import logger
import pandas as pd
from Constants.constants import host_name,user_name,user_password,host_name,message_status
from Exception_Handler.exception_handler import Exception_Handling
exception_handling=Exception_Handling()

class Database():
    database_name = 'IMQ'

    def __init__(self):
        self.connect()
        self.create_database()
        self.use_database(self.database_name)

    def connect(self):
        self.mydb = mysql.connector.connect(
            host=host_name,
            user=user_name,
            password=user_password,
        )

    def use_database(self, database_name):
        query = 'USE '+database_name
        self.execute(query)

    def execute(self, query):
        try:
            self.mydb.start_transaction()
            self.mycursor.execute(query)
            self.mydb.commit()
        except mysql.connector.Error as error:
            exception_handling.database_query_execution_error(str(error))


    def create_database(self):
        self.mycursor = self.mydb.cursor()
        query = "CREATE DATABASE IF NOT EXISTS "+self.database_name
        self.execute(query)

    def create_clients_reference_table(self):
        query = 'Create table IF NOT EXISTS Root_Table (ID INT AUTO_INCREMENT PRIMARY KEY, client_address VARCHAR(255) NOT NULL UNIQUE,client_name VARCHAR(255) NOT NULL)'
        self.execute(query)
        query = 'Create table IF NOT EXISTS Messages(ClientID VARCHAR(255),ServerID VARCHAR(255),TimeSent datetime,MessageSent VARCHAR(255), Topic_id VARCHAR(255), Message_status VARCHAR(255))'
        self.execute(query)
        query = 'Create table IF NOT EXISTS Topic (ID INT AUTO_INCREMENT PRIMARY KEY, topic_name VARCHAR(255) NOT NULL, client_id VARCHAR(255) NOT NULL)'
        self.execute(query)
        query = 'Create table IF NOT EXISTS `subscriber`(`channel_id` VARCHAR(255), `client_id` VARCHAR(255), `Time` datetime)'
        self.execute(query)
    
    def insert_into_reference_table(self, client_address,user_name):
        query = "INSERT into `Root_Table` (`client_address`,`client_name`) values('" + client_address+"','"+user_name+"')"
        self.execute(query)
    
    def update_message_table(self,choosen_id,reply):
        query="select `client_id` from `subscriber` where `channel_id` = '"+str(choosen_id)+"';"
        id=pd.read_sql_query(query,self.mydb)
        Subscriber_id=id['client_id'].values
        self.mydb.commit()
        for i in Subscriber_id:
            query="Insert into `Messages` (`ClientID`,`ServerID`,`TimeSent`,`MessageSent`,`Topic_id`,`Message_status`) values ('"+i+"','"+str(host_name)+"',now(),'"+str(reply)+"','"+str(choosen_id)+"','1')"
            self.execute(query)
        status="Message has been published in channel ID : "+str(choosen_id)+".\n"
        return status
        
    def update_last_seen(self,user_name,channel_ID):
        query="select `ID` from `Root_Table` where `client_name` = '"+user_name+"';"
        id=pd.read_sql_query(query,self.mydb)
        id_new=id['ID'].values[0]
        self.mydb.commit()
        query="update `subscriber` set `Time`= now() where `client_id` = '"+str(id_new)+"' and `channel_id` =  '"+str(channel_ID)+"';"
        self.execute(query)
        query="update `Messages` set `Message_status`= '0' where `ClientID` = '"+str(id_new)+"' and `Topic_id` =  '"+str(channel_ID)+"';"
        self.execute(query)
        
    def creating_channel(self,channel_name,user_name):
        query="select `ID` from `Root_Table` where `client_name` = '"+user_name+"';"
        id=pd.read_sql_query(query,self.mydb)
        id_new=id['ID'].values[0]
        self.mydb.commit()
        query3 = "INSERT into `Topic` (`topic_name`,`client_id`) values ('"+channel_name+"','"+str(id_new)+"')"
        self.execute(query3)
        status="\nChannel "+str(channel_name)+" has been created by admin "+user_name+".\n"
        return status

    def show_list_of_channel(self):
        mycursor = self.mydb.cursor()
        query="select `topic_name`,`ID` from Topic"
        self.mycursor.execute(query)
        result="TOPIC_NAME  TOPIC_ID\n=================\n"
        for i in mycursor:
            result=result+str(i[0])+"      "+str((i[1]))+"\n"

        return result
    
    def add_subscriber(self,user_name,channel_id):
        query="select `ID` from `Root_Table` where `client_name` = '"+user_name+"';"
        id=pd.read_sql_query(query,self.mydb)
        id_new=id['ID'].values[0]
        self.mydb.commit()
        query3 = "INSERT into `subscriber` (`channel_id`,`client_id`,`Time`) values ('"+str(channel_id)+"','"+str(id_new)+"',now())"
        self.execute(query3)
        result="\nChannel ID "+str(channel_id)+" has been subscribed by client "+user_name+".\n"
        return result

    def return_client_id(self,user_name):
        query="select `ID` from `Root_Table` where `client_name` = '"+user_name+"';"
        id=pd.read_sql_query(query,self.mydb)
        id_new=id['ID'].values[0]
        return int(id_new)

    def return_last_seen(self,channel_ID,client_id):
        query="select `Time` from `subscriber` where  `Client_ID` = '"+str(client_id)+"' and `channel_id` =  '"+str(channel_ID)+"';"
        id=pd.read_sql_query(query,self.mydb)
        Time=id['Time'].values
        self.mydb.commit()
        return Time

    def return_subscribed_channels(self,user_name):
        client_id=self.return_client_id(user_name)
        query="select distinct `channel_id` from `subscriber` where  `Client_ID` = '"+str(client_id)+"';"
        id=pd.read_sql_query(query,self.mydb)
        subscribers=id['channel_id'].values
        self.mydb.commit()
        result="TOPIC_ID \n=========\n"
        for i in subscribers:
            result=result+str(i)+"\n"
        return result







    

    