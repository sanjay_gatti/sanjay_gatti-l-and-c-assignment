from Protocols.imqprtocol import ImqProtocol
from dataclasses import dataclass

@dataclass
class Response(ImqProtocol):
    message_status: str = "Delivered"