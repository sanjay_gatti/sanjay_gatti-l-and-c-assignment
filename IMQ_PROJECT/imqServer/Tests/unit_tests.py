import unittest
import sys
sys.path.append('../')
from Sockets.sockethandler import Socket_Handler
from Database.database import Database
from Server.server import server_socket

class Test(unittest.TestCase):
    server_obj=Socket_Handler()
    DB_object=Database()
    server_socket_obj=server_socket()

    def test_for_socket_creation(self):
        self.assertTrue(type(self.server_obj.create_socket))
    
    def test_for_socket_bind(self):
        self.server_obj.create_socket()
        self.assertTrue(type(self.server_obj.socket_bind()))

    def test_for_socket_listening(self):
        self.server_obj.create_socket()
        self.server_obj.socket_bind()
        self.assertTrue(type(self.server_obj.socket_listen()))
    
    def test_show_list_of_channel(self):
        self.assertIsNotNone(self.DB_object.show_list_of_channel())
    
    def test_creating_channel(self):
        user_name='SAN'
        channel_Name='SPORTS'
        result="\nChannel SPORTS has been created by admin SAN.\n"
        self.assertEqual(result,self.DB_object.creating_channel(channel_Name,user_name))

    def test_add_subscriber(self):
        user_name='SAN'
        channel_id=1
        result="\nChannel ID 1 has been subscribed by client SAN.\n"
        self.assertEqual(result,self.DB_object.add_subscriber(user_name,channel_id))
    
    def test_data_encode(self):
        original_data="Connection made with 192.168.137.1_60950"
        result='"{\\"data\\": \\"Connection made with 192.168.137.1_60950\\", \\"version\\": 5, \\"format\\": \\"json\\", \\"message_status\\": \\"Delivered\\"}"'
        self.assertEqual(result,self.server_socket_obj.data_encode(original_data))

    def test_data_decode(self):
        encoded_data=b'"{\\"data\\": \\"SAN\\", \\"version\\": 5, \\"format\\": \\"json\\", \\"request_type\\": \\"connect_to_topic\\"}"'
        result='SAN'
        self.assertEqual(result,self.server_socket_obj.data_decode(encoded_data))

    def test_data_encode_notequal(self):
        original_data="Connection made with 192.168.137.1_60950"
        result='"{\"data\\": \\"Connection made with 192.168.137.1_60950\\", \\"version\\": 5, \\"format\\": \\"json\\", \\"message_status\\": \\"Delivered\\"}"'
        self.assertNotEqual(result,self.server_socket_obj.data_encode(original_data))

    def test_data_decode_notequal(self):
        encoded_data=b'"{\\"data\\": \\"SAN\\", \\"version\\": 5, \\"format\\": \\"json\\", \\"request_type\\": \\"connect_to_topic\\"}"'
        result='SPAN'
        self.assertNotEqual(result,self.server_socket_obj.data_decode(encoded_data))


if __name__ == '__main__':
    unittest.main()