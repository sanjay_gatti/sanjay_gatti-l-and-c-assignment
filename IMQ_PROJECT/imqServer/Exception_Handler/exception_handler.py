from Log.log import logger
class Exception_Handling:

    def create_socket_exception(self,error):
        print(str(error))
        logger.exception(str(error))

    def connect_socket_exception(self,error):
        print(str(error))
        logger.exception(str(error))   

    def recv_response_exception(self,error):
        print(str(error))
        logger.exception(str(error))

    def send_response_exception(self,error):
        print(str(error))
        logger.exception(str(error))

    def socket_bind_exception(self,error):
        print(str(error))
        logger.exception(str(error))

    def socket_listen_exception(self,error):
        print(str(error))
        logger.exception(str(error))

    def socket_accept_exception(self,error):
        print(str(error))
        logger.exception(str(error))

    def connection_exception(self,error):
        print("Connection terminated from client side\n")
        logger.exception(str(error))
    
    def database_query_execution_error(self,error):
        print("Something went wrong while executing the query: {}".format(error))
        logger.exception("Something went wrong while executing the query: {}".format(error))
    
    