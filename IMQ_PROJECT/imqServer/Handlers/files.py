import datetime
import time
import os.path

class File():
    def write_to_file(self,address,data):
        timestamp = time.time()
        connection_time = datetime.datetime.fromtimestamp(
            timestamp).strftime('%Y-%m-%d::%H:%M:%S - ')

        if os.path.exists(address[0] + '_' + str(address[1]) + '.txt'):
            with open('{0}_{1}.txt'.format(address[0], address[1]), "a") as file:
                file.write(connection_time + str(data) + '\n')
        else:
            with open('{0}_{1}.txt'.format(address[0], address[1]), "w") as file:
                file.write(connection_time + str(data) + '\n')