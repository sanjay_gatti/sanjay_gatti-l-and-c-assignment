from Log.log import logger
class Exception_Handling:
    def value_error(self,error):
        print("Enter a valid number\n")
        logger.exception(str(error))

    def create_socket_exception(self,error):
        logger.exception(str(error))

    def connect_socket_exception(self,error):
        logger.exception(str(error))   

    def recv_response_exception(self,error):
        logger.exception(str(error))

    def send_response_exception(self,error):
        logger.exception(str(error))

    def close_socket_exception(self,error):
        print(error)
        logger.exception(str(error))
    
    