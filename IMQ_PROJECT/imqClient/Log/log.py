import logging

logger = logging.getLogger(__name__)
#logger.setLevel(logging.DEBUG)
logging_formatter =  logging.Formatter('%(asctime)s:%(name)s:%(message)s')
file_handler =  logging.FileHandler('error_log.log')
file_handler.setLevel(logging.ERROR)
file_handler.setFormatter(logging_formatter)
logger.addHandler(file_handler)