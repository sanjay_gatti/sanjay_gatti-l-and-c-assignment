import sys
sys.path.append('../')
from Sockets.sockethandler import Socket_Handler
from Constants.constants import admin_key,create_channel,show_channel_list,subscrib_channel,publish_channel,show_messages,subscriber_list
from Exception_Handler.exception_handler import Exception_Handling
exception_handling=Exception_Handling()

class Cleint_Services:

    def choose_role(self,address,name,client_object):
        print("User Name : "+str(name)+" \n1. Enter 1 to login as admin. \n2. Enter 2 to login as user. \n")
        try:
            choice= (int(input()))
            if( choice== 1):
                self.validate_admin(address,name,client_object)
            elif( choice ==2) :
                choice=self.choose_option()
                self.select_service(choice,address,name,client_object)
            else:
                print("Invalid Choice\n")
        except ValueError as e:
            exception_handling.value_error(str(e))


    def choose_option(self):
        try:
            print("\nChoose below options :\n 1. Subscribe to Channel\n 2. Publish to Channel\n 3. Show My Messages\n")
            return(int(input()))
        except ValueError as e:
            exception_handling.value_error(str(e))
            return(int(99))
    
    def select_service(self,choice,address,name,client_object):
        try:
            if choice == 1:
                self.subscribe(address,name,client_object)
            elif choice == 2:
                self.publish(address,name,client_object)
            elif choice == 3:
                self.show_messages(name,client_object)
            elif choice == 99:
                pass
            else:
                print("Invalid choice !!\n")
        except ValueError as e:
            exception_handling.value_error(str(e))
        
    def validate_admin(self,address,name,client_object):
        print("Enter the admin key\n")
        key=str(input())
        if(str(key)==admin_key):
            self.create_channel(address,name,client_object)
            print("\n")
        else:
            print("NOT A ADMIN\n")
            client_object.close_connection()
            exit(0)
            
    def create_channel(self,address,name,client_object):
        print("Enter the channel name\n")
        channel_name=input()
        channel_name=create_channel+channel_name
        client_object.send_command(channel_name)
        client_object.receive_response()
    
    def subscribe(self,address,name,client_object):
        print("Here is the list of channels available for you !!\n")
        client_object.send_command(show_channel_list)
        client_object.receive_response()
        print("Enter the channel ID :\n")
        channel_id=str(input())
        command=subscrib_channel+channel_id
        client_object.send_command(command)
        client_object.receive_response()
    
    def publish(self,address,user_name,client_object):
        print("Here is the list of channels available for you !!\n")
        client_object.send_command(show_channel_list)
        client_object.receive_response()
        print("Enter the channel ID :\n")
        channel_id=str(input())
        print("Enter the Message :\n")
        message=input()
        print("\n")
        command=publish_channel+str(channel_id)+':'+(message)
        client_object.send_command(command)
        client_object.receive_response()

    def show_messages(self,name,client_object):
        print("List of channels available for you!!\n")
        client_object.send_command(subscriber_list)
        client_object.receive_response()
        print("Select the Channel ID !!\n")
        Channel_ID=input()
        print("These are your latest messages "+name+"!!\n")
        command=show_messages+name+':'+str(Channel_ID)
        client_object.send_command(command)
        client_object.receive_response()


        

