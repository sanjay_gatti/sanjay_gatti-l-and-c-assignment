from dataclasses import dataclass

@dataclass
class ImqProtocol :
    data : str
    version : int = 5
    format : str = "json"