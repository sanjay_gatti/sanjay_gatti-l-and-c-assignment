from Protocols.imqprtocol import ImqProtocol
from dataclasses import dataclass

@dataclass
class Request(ImqProtocol):
    request_type: str = "connect_to_topic"