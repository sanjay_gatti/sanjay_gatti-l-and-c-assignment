from Protocols.imqprtocol import ImqProtocol
import jsonpickle
import json

class Parser:
    def get_json_encoded(self,header):
        return json.dumps(jsonpickle.encode(header, unpicklable=False), indent=4)
    
    def get_json_decoded(self,header):
        header=json.loads(header)
        return json.loads(header)
